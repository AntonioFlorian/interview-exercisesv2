var _ = require('lodash');

export const detectSums = (array) => {
  if (typeof(array) === "object") {
        //Creating the empty array that will receive objects
        const Results = [];
        //Creating 2 for loops that will go over the array and add them to each other
        for (let i = 0; i < array.length; i++) {
          let pA = array[i];
          for (let j = 0; j < array.length; j++) {
            let pB = array[j]
            let sum = pA + pB
            let pAindex = array.indexOf(pA)
            let pBindex = array.indexOf(pB)
            let sumIndex = array.indexOf(sum)
            //in the if we are searching the array for the sum to be present, and if it is we push it into the empty array we created
            if (sumIndex !== -1 && pAindex !== pBindex && sumIndex !==pAindex && sumIndex !== pBindex) {
                if (Results.some(el => _.isEqual(el, {pA : pAindex, pB: pBindex, sum: sumIndex}) || _.isEqual(el, {pA : pBindex, pB: pAindex, sum: sumIndex}))) {
                } else {
                  Results.push({pA : pAindex, pB: pBindex, sum: sumIndex});
                }
            }
          }
        }
        console.log(typeof(Results))
        return Results
      } else {
        throw new Error('Input is not an array'); 
      }
};

export function calculateResult(input) {
  const parsedInput = input.split('').map(i => parseInt(i.trim(), 10));
  let error = null;
  let result = ""
  try {
    result = JSON.stringify(detectSums(parsedInput));
  } catch (e) {
    error = e.message;
  }
  return { input: parsedInput, result, error }
}
